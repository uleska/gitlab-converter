from setuptools import setup

setup(
    name="gitlab-converter",
    version="1.0.0",
    description="Takes GitLab vulnerability model and converts it into the correct uleska custom tools format",
    author="Uleska",
    author_email="developers@uleska.com",
    license="MIT License",
    zip_safe=False,
    keywords="uleska",
    packages=["converter"],
)
