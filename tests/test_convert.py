import unittest
import os
from uleska.vulnerability import Vulnerability, Severity
from converter import SAST_convert


class TestConvert(unittest.TestCase):
    def test_SAST_convert_returns_no_vulns_when_file_is_not_found(self):
        # given
        file_location = "/tmp/not-a-file.json"

        # when
        vulns = SAST_convert(file_location)

        # then
        self.assertEquals(0, len(vulns))

    def test_SAST_convert_returns_no_vulns_when_file_is_not_json(self):
        # given
        file_location = example_files_location() + "text.txt"

        # when
        vulns = SAST_convert(file_location)

        # then
        self.assertEquals(0, len(vulns))

    def test_SAST_convert_returns_no_vulns_when_file_has_bad_perissions(self):
        # given
        file_location = example_files_location() + "bad-permission.json"
        os.chmod(file_location, 0o000)  # this changes file so no one can read it

        # when
        vulns = SAST_convert(file_location)

        # then
        self.assertEquals(0, len(vulns))
        os.chmod(file_location, 0o664)  # this changes file back to normal

    def test_SAST_convert_returns_no_vulns_when_file_has_no_vulns(self):
        # given
        file_location = example_files_location() + "no-vulns.json"

        # when
        vulns = SAST_convert(file_location)

        # then
        self.assertEquals(0, len(vulns))

    def test_SAST_convert_returns_DDOS_when_file_that_has_ddos(self):
        # given
        file_location = example_files_location() + "ddos-vuln.json"

        # when
        vulns = SAST_convert(file_location)

        # then
        self.assertEquals(1, len(vulns))

        expected = Vulnerability(
            "Incorrect Regular Expression",
            summary="RegExp() called with a variable, this might allow an attacker to DOS your application with a long-running regular expression.\n",
            source="coverage/lcov-report/prettify.js",
            severity=Severity.MEDIUM,
            cwe="CWE-185",
        )
        self.assert_vulns_equal(expected, vulns[0])

    def test_SAST_convert_returns_two_vulns_when_file_that_has_two_vulns(self):
        # given
        file_location = example_files_location() + "two-vulns.json"

        # when
        vulns = SAST_convert(file_location)

        # then
        self.assertEquals(2, len(vulns))

        expected1 = Vulnerability(
            "Incorrect Regular Expression",
            summary="RegExp() called with a variable, this might allow an attacker to DOS your application with a long-running regular expression.\n",
            source="coverage/lcov-report/prettify.js",
            severity=Severity.MEDIUM,
            cwe="CWE-185",
        )
        expected2 = Vulnerability(
            "Incorrect Regular Expression",
            summary="RegExp() called with a variable, this might allow an attacker to DOS your application with a long-running regular expression.\n",
            source="src/components/dashboard/roleDashboards/appManager/applicationTab/settings/Tools/ToolkitToolConfiguration.tsx",
            severity=Severity.MEDIUM,
            cwe="CWE-185",
        )

        self.assert_vulns_equal(expected1, vulns[0])
        self.assert_vulns_equal(expected2, vulns[1])

    def test_SAST_convert_returns_vuln_with_no_message(self):
        # given
        file_location = example_files_location() + "vuln-with-no-message.json"

        # when
        vulns = SAST_convert(file_location)

        # then
        self.assertEquals(1, len(vulns))

        expected = Vulnerability(
            "Unknown vulnerability",
            summary="RegExp() called with a variable, this might allow an attacker to DOS your application with a long-running regular expression.\n",
            source="coverage/lcov-report/prettify.js",
            severity=Severity.MEDIUM,
            cwe="CWE-185",
        )
        self.assert_vulns_equal(expected, vulns[0])

    def test_SAST_convert_returns_vuln_with_no_description(self):
        # given
        file_location = example_files_location() + "vuln-with-no-description.json"

        # when
        vulns = SAST_convert(file_location)

        # then
        self.assertEquals(1, len(vulns))

        expected = Vulnerability(
            "Incorrect Regular Expression",
            summary="",
            source="coverage/lcov-report/prettify.js",
            severity=Severity.MEDIUM,
            cwe="CWE-185",
        )
        self.assert_vulns_equal(expected, vulns[0])

    def test_SAST_convert_returns_critical_severity(self):
        # given
        file_location = example_files_location() + "vuln-critical-severity.json"

        # when
        vulns = SAST_convert(file_location)

        # then
        self.assertEquals(1, len(vulns))

        expected = Vulnerability(
            "Incorrect Regular Expression",
            summary="RegExp() called with a variable, this might allow an attacker to DOS your application with a long-running regular expression.\n",
            source="coverage/lcov-report/prettify.js",
            severity=Severity.HIGH,
            cwe="CWE-185",
        )
        self.assert_vulns_equal(expected, vulns[0])

    def test_SAST_convert_returns_high_severity(self):
        # given
        file_location = example_files_location() + "vuln-high-severity.json"

        # when
        vulns = SAST_convert(file_location)

        # then
        self.assertEquals(1, len(vulns))

        expected = Vulnerability(
            "Incorrect Regular Expression",
            summary="RegExp() called with a variable, this might allow an attacker to DOS your application with a long-running regular expression.\n",
            source="coverage/lcov-report/prettify.js",
            severity=Severity.HIGH,
            cwe="CWE-185",
        )
        self.assert_vulns_equal(expected, vulns[0])

    def test_SAST_convert_returns_low_severity(self):
        # given
        file_location = example_files_location() + "vuln-low-severity.json"

        # when
        vulns = SAST_convert(file_location)

        # then
        self.assertEquals(1, len(vulns))

        expected = Vulnerability(
            "Incorrect Regular Expression",
            summary="RegExp() called with a variable, this might allow an attacker to DOS your application with a long-running regular expression.\n",
            source="coverage/lcov-report/prettify.js",
            severity=Severity.LOW,
            cwe="CWE-185",
        )
        self.assert_vulns_equal(expected, vulns[0])

    def test_SAST_convert_returns_unknown_severity(self):
        # given
        file_location = example_files_location() + "vuln-unknown-severity.json"

        # when
        vulns = SAST_convert(file_location)

        # then
        self.assertEquals(1, len(vulns))

        expected = Vulnerability(
            "Incorrect Regular Expression",
            summary="RegExp() called with a variable, this might allow an attacker to DOS your application with a long-running regular expression.\n",
            source="coverage/lcov-report/prettify.js",
            severity=Severity.UNKNOWN,
            cwe="CWE-185",
        )
        self.assert_vulns_equal(expected, vulns[0])

    def test_SAST_convert_returns_no_severity(self):
        # given
        file_location = example_files_location() + "vuln-no-severity.json"

        # when
        vulns = SAST_convert(file_location)

        # then
        self.assertEquals(1, len(vulns))

        expected = Vulnerability(
            "Incorrect Regular Expression",
            summary="RegExp() called with a variable, this might allow an attacker to DOS your application with a long-running regular expression.\n",
            source="coverage/lcov-report/prettify.js",
            severity=Severity.UNKNOWN,
            cwe="CWE-185",
        )
        self.assert_vulns_equal(expected, vulns[0])

    def test_SAST_convert_for_flawfinder(self):
        # given
        file_location = example_files_location() + "flaw-vuln.json"

        # when
        vulns = SAST_convert(file_location)

        # then
        self.assertEquals(1, len(vulns))

        expected = Vulnerability(
            "If format strings can be influenced by an attacker, they can be exploited (CWE-134).",
            source="flaw.c",
            severity=Severity.HIGH,
            recommendation="Use a constant for the format specification.",
            cwe="CWE-134",
        )
        self.assert_vulns_equal(expected, vulns[0])

    def assert_vulns_equal(
        self, expected: Vulnerability, result: Vulnerability
    ) -> None:
        self.assertEquals(expected.title, result.title)
        self.assertEquals(expected.summary, result.summary)
        self.assertEquals(expected.source, result.source)
        self.assertEquals(expected.severity, result.severity)
        self.assertEquals(expected.cwe, result.cwe)
        self.assertEquals(expected.recommendation, result.recommendation)


def example_files_location() -> str:
    return os.path.dirname(os.path.realpath(__file__)) + "/example-files/"
