import json
from typing import Optional
from uleska.vulnerability import Vulnerability, Severity

# Mandintory fields that according to gitlab will be present
VULNERABILITIES = "vulnerabilities"
VULNERABILITIES_LOCATION = "location"
VULNERABILITIES_IDENTIFIERS = "identifiers"
VULNERABILITIES_IDENTIFIERS_TYPE = "type"
VULNERABILITIES_IDENTIFIERS_NAME = "name"

# Optional fields that might not be there
OPTIONAL_VULNERABILITIES_MESSAGE = "message"
OPTIONAL_VULNERABILITIES_DESCRIPTION = "description"
OPTIONAL_VULNERABILITIES_SEVERITY = "severity"
OPTIONAL_VULNERABILITIES_LOCATION_FILE = "file"
OPTIONAL_VULNERABILITIES_SOLUTION = "solution"


def SAST_convert(file_location: str) -> [Vulnerability]:
    """
    Converts a file in the follow format to Uleska Vulns
    https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/master/dist/sast-report-format.json
    """
    gitlab_vulns = _git_lab_vulns_from_file_(file_location)
    return list(map(lambda vuln: _git_lab_vuln_to_uleska_vuln_(vuln), gitlab_vulns))


def _git_lab_vulns_from_file_(file_location: str) -> [dict]:
    try:
        with open(file_location, "r") as json_file:
            data = json.load(json_file)
            return data.get(VULNERABILITIES)
    except OSError as e:
        print(
            f"""Could not open file {file_location}.
        Because: {e.strerror}"""
        )
        return []
    except json.JSONDecodeError as e:
        print(
            f"""File {file_location} is not in json format.
        Because: {e}"""
        )
        return []


def _git_lab_vuln_to_uleska_vuln_(gitlab_vuln: dict) -> Vulnerability:
    return Vulnerability(
        gitlab_vuln.get(OPTIONAL_VULNERABILITIES_MESSAGE, "Unknown vulnerability"),
        summary=gitlab_vuln.get(OPTIONAL_VULNERABILITIES_DESCRIPTION, ""),
        recommendation=gitlab_vuln.get(OPTIONAL_VULNERABILITIES_SOLUTION, ""),
        source=_find_source_(gitlab_vuln),
        severity=_find_severity_(gitlab_vuln),
        cwe=_find_cwe_(gitlab_vuln),
    )


def _find_source_(gitlab_vuln: dict) -> str:
    location: dict = gitlab_vuln.get(VULNERABILITIES_LOCATION)
    return location.get(OPTIONAL_VULNERABILITIES_LOCATION_FILE, "")


def _find_cwe_(gitlab_vuln: dict) -> Optional[str]:
    first_cwe_element = next(
        filter(
            lambda identifier: identifier.get(VULNERABILITIES_IDENTIFIERS_TYPE)
            == "cwe",
            gitlab_vuln.get(VULNERABILITIES_IDENTIFIERS),
        ),
        None,
    )
    if first_cwe_element is not None:
        return first_cwe_element.get(VULNERABILITIES_IDENTIFIERS_NAME)
    return None


def _find_severity_(gitlab_vuln: dict) -> Severity:
    severity_string = gitlab_vuln.get(OPTIONAL_VULNERABILITIES_SEVERITY, "Unknown")

    if severity_string in ("Critical", "High"):
        return Severity.HIGH
    if severity_string == "Medium":
        return Severity.MEDIUM
    if severity_string == "Low":
        return Severity.LOW
    if severity_string == "Info":
        return Severity.INFORMATIONAL

    return Severity.UNKNOWN
